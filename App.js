import 'react-native-gesture-handler';

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './res/components/HomeScreen';
import DetailsScreen from './res/components/DetailsScreen';
import CreateScreen from './res/components/CreateScreen';

const App = () => {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name={"Home"}
          component={HomeScreen}
        />
        <Stack.Screen
          name={"Details"}
          component={DetailsScreen}
        />
        <Stack.Screen
          name={"Create"}
          component={CreateScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
