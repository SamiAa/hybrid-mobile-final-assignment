import React, { useEffect, useRef, useState } from 'react';
import { Button, View, Text } from 'react-native';
import CustomButton from './CustomButton';

const HomeScreen = ({ navigation, route }) => {
    const data = [
        {
            title: "Käy kaupassa",
            details: "Osta maitoa, mehua, jauhelihaa ja hedelmiä.",
            deadline: "15.12.2020"
        },
        {
            title: "Osta uusi imuri",
            details: "Yksi mahdollisuus olisi Electrolux P500 - testivoittaja.",
            deadline: "12.2.2021"
        },
        {
            title: "Myy vanha puhelin",
            details: "Muista tyhjentää puhelimen muisti ennen luovutusta.",
            deadline: "22.1.2021"
        }
    ];

    // The "database" of this application.
    const [toDoList, setToDoList] = useState(data);

    // This tells which item in the toDoList should 
    // show its details at the moment.
    const [activeIndex, setActiveIndex] = useState(null);

    // Toggle toDoLists certain items "show" attribute.
    // This tells if details of certain item could be seen.
    const showDetails = (index) => {
        if (index === activeIndex) {
            setActiveIndex(null);
        } else {
            setActiveIndex(index);
        }
    };

    // Delete item from toDoList.
    const deleteItem = (index) => {
        const toDoListCopy = toDoList;
        toDoListCopy.splice(index, 1);
        setActiveIndex(null);
        setToDoList(toDoListCopy);
    };

    // Sort the toDoList so that the most urgent items 
    // are at the beginning of the list.
    const updateAndSortToDos = () => {
        const toDoListCopy = toDoList;
        let latestDeadline = {};
        let currentDeadline = {};
        let splitDeadline = [];
        let indexToUpdate = 0;
        let memory = {};
        let swapped = false;
        let isMoreUrgent = false;

        // Update the toDoListCopy with given values 
        // if editedDetails parameter is defined.
        if (route.params !== undefined) {
            if (route.params.function === "Update") {
                toDoListCopy[activeIndex] = route.params.editedDetails;
            } else if (route.params.function === "Add") {
                toDoListCopy.push(route.params.details);
                setActiveIndex(null);
            }
        }

        do {
            swapped = false;
            indexToUpdate = 0;
            // Go through the toDoListCopy and swap the current object
            // with the object before to sort the list.
            for (let i = 1; i < toDoListCopy.length; i++) {
                // Set the latestDeadline as the deadline 
                // of the object in the index before.
                splitDeadline = toDoListCopy[i - 1].deadline.split(".");
                latestDeadline = {
                    day: parseInt(splitDeadline[0]),
                    month: parseInt(splitDeadline[1]),
                    year: parseInt(splitDeadline[2])
                };

                // Set the current deadline as the deadline 
                // of the current object.
                splitDeadline = toDoListCopy[i].deadline.split(".");
                currentDeadline = {
                    day: parseInt(splitDeadline[0]),
                    month: parseInt(splitDeadline[1]),
                    year: parseInt(splitDeadline[2])
                };

                // If the currentDeadline comes before the latestDeadline
                // then it should be set as more urgent.
                if (currentDeadline.year === latestDeadline.year) {
                    if (currentDeadline.month === latestDeadline.month) {
                        if (currentDeadline.day < latestDeadline.day) {
                            isMoreUrgent = true;
                        }
                    } else if (currentDeadline.month < latestDeadline.month) {
                        isMoreUrgent = true
                    }
                } else if (currentDeadline.year < latestDeadline.year) {
                    isMoreUrgent = true;
                }

                // Do the swap if the currentDeadline is more urgent.
                if (isMoreUrgent) {
                    memory = toDoListCopy[indexToUpdate];
                    toDoListCopy[indexToUpdate] = toDoListCopy[i];
                    toDoListCopy[i] = memory;
                    swapped = true;
                }

                indexToUpdate++;
                isMoreUrgent = false;
            }

            // Set the toDoList as the sorted array of objects.
            setToDoList(toDoListCopy);
        } while (swapped)
    };

    // Update and sort only once when the app is launched 
    // and after making changes into the toDoList.
    const firstRender = useRef(true);
    const lastParams = useRef(null);
    if (firstRender.current) {
        updateAndSortToDos();
        firstRender.current = false;
    } else {
        if (route.params !== undefined && 
            route.params.function !== undefined) {
                if (route.params.function === "Update" && 
                    route.params.editedDetails !== lastParams.current) {
                        updateAndSortToDos();
                        lastParams.current = route.params.editedDetails;
                } else if (route.params.function === "Add" && 
                    route.params.details !== lastParams.current) {
                        updateAndSortToDos();
                        lastParams.current = route.params.details;
                }
        }
    }

    return (
        <View style={{ flex: 1 }}>
            {/* Header */}
            {/* <View style={styles.header}>
                <View style={[styles.headerLayer, styles.headerLayerSteelBlue, { elevation: 15 }]}></View>
                <View style={[styles.headerLayer, styles.headerLayerSkyBlue, { elevation: 10 }]}></View>
                <View style={[styles.headerLayer, styles.headerLayerPowderBlue, { elevation: 5 }]}></View>
            </View> */}

            {/* MainContent */}
            <View style={styles.mainContent}>
                {/* <Text style={[styles.fontLarge, styles.bold, styles.marginLarge, styles.textShadow]}>ToDo App</Text> */}
                {
                    toDoList.map((item, i) => <View style={[styles.listItem]} key={i}> 
                        <Button title={item.title} onPress={() => showDetails(i)} />
                        {i === activeIndex ?
                            <View style={[styles.center, { padding: 8 }]}>
                                <Text>{item.details}</Text>
                                <Text style={[styles.bold, { marginBottom: 16 }]}>{item.deadline}</Text>
                                <View style={{ width: "33.33%" }}>
                                    <Button title={"Edit"} color={"green"} 
                                        onPress={() => {
                                            navigation.navigate('Details', {
                                                title: toDoList[i].title,
                                                details: toDoList[i].details,
                                                deadline: toDoList[i].deadline
                                            });
                                        }}
                                    />
                                    <Button title={"Delete"} color={"red"} onPress={() => deleteItem(i)}
                                    />
                                </View>
                            </View> : 
                            null}
                    </View>)
                }
                <CustomButton title={"+"} pos={{ bottom: -45, right: 32 }} onPress={() => {
                    navigation.navigate('Create');
                }} />
            </View>

            {/* Footer */}
            <View style={styles.footer}>
                <View style={[styles.headerLayer, styles.headerLayerPowderBlue, { elevation: 5 }]}></View>
                <View style={[styles.headerLayer, styles.headerLayerSkyBlue, { elevation: 10 }]}></View>
                <View style={[styles.headerLayer, styles.headerLayerSteelBlue, { elevation: 15 }]}></View>
            </View>
        </View>
    );
};

const styles = {
    header: {
        flex: 1,
        flexDirection: "column",
    },
    mainContent: {
        flex: 4,
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "center",
        paddingBottom: 32,
        margin: 8
    },
    footer: {
        flex: 1,
        flexDirection: "column",
        alignItems: "flex-start"
    },
    headerLayer: {
        flex: 1,
    },
    headerLayerSteelBlue: {
        width: "100%",
        backgroundColor: "steelblue"
    },
    headerLayerSkyBlue: {
        width: "66.66%",
        backgroundColor: "skyblue"
    },
    headerLayerPowderBlue: {
        width: "33.33%",
        backgroundColor: "powderblue"
    },
    listItem: {
        boxSizing: "border-box",
        width: "100%",
        paddingTop: 1,
        paddingBottom: 1
    },
    textShadow: {
        textShadowColor: "rgba(180, 200, 205, 0.7)",
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10
    },
    center: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    fontLarge: {
        fontSize: 24
    },
    bold: {
        fontWeight: "bold"
    },
    marginSmall: {
        margin: 8
    },
    marginMedium: {
        margin: 16
    },
    marginLarge: {
        margin: 32
    }
}

export default HomeScreen;