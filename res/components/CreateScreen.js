import React, { useState } from 'react';
import { Button, Text, View } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

const CreateScreen = ({ navigation, route }) => {
    const [title, setTitle] = useState("Title");
    const [details, setDetails] = useState("Details");
    const [deadline, setDeadline] = useState("Deadline");

    const params = {
        function: "Add",
        details: {
            title: title,
            details: details,
            deadline: deadline
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.contentContainer}>
                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Title</Text>
                    <TextInput 
                        onChangeText={newTitle => setTitle(newTitle)} 
                        defaultValue={title} />
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Details</Text>
                    <TextInput 
                        onChangeText={newDetails => setDetails(newDetails)} 
                        defaultValue={details} />
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Deadline</Text>
                    <TextInput 
                        onChangeText={newDeadline => setDeadline(newDeadline)} 
                        defaultValue={deadline} />
                </View>
                <View style={styles.buttonContainer}>
                    <Button title={"OK"} onPress={() => navigation.navigate('Home', params)} color={"green"} />
                    <Button title={"Cancel"} onPress={() => navigation.goBack()} color={"red"} />
                </View>
            </View>

            {/* Footer */}
            <View style={styles.footer}>
                <View style={[styles.headerLayer, styles.headerLayerPowderBlue, { elevation: 5 }]}></View>
                <View style={[styles.headerLayer, styles.headerLayerSkyBlue, { elevation: 10 }]}></View>
                <View style={[styles.headerLayer, styles.headerLayerSteelBlue, { elevation: 15 }]}></View>
            </View>
        </View>
    );
};

const styles = {
    container: {
        flex: 1
    },
    label: {
        fontSize: 14,
        color: "grey"
    },
    contentContainer: {
        flex: 5,
        flexDirection: "column",
        alignItems: "center",
        margin: 8
    },
    buttonContainer: {
        width: "33.33%",
        marginTop: 32,
    },
    inputContainer: {
        width: "100%"
    },
    footer: {
        flex: 1,
        flexDirection: "column",
        alignItems: "flex-end"
    },
    headerLayer: {
        flex: 1,
    },
    headerLayerSteelBlue: {
        width: "100%",
        backgroundColor: "steelblue"
    },
    headerLayerSkyBlue: {
        width: "66.66%",
        backgroundColor: "skyblue"
    },
    headerLayerPowderBlue: {
        width: "33.33%",
        backgroundColor: "powderblue"
    }
};

export default CreateScreen;