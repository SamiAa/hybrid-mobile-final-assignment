import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';

const CustomButton = (props) => {
    return (
        <TouchableOpacity onPress={props.onPress}
            style={{position: "absolute", bottom: props.pos.bottom, right: props.pos.right}}>
                <View style={[styles.button]}>
                    <Text style={styles.buttonText}>{props.title}</Text>
                </View>
        </TouchableOpacity>
    );
};

const styles = {
    button: {
        backgroundColor: "orange",
        paddingVertical: 8,
        paddingHorizontal: 18,
        borderRadius: 25,
        elevation: 15
    },
    buttonText: {
        color: "white",
        fontSize: 24
    }
};

export default CustomButton;